For the most up-to-date and accurate documentation, see: https://www.wiki.ed.ac.uk/display/ResearchServices/Bioinformatics.

Everything below this is maintained for archive and may be out of date.

# Nextflow on Eddie

[Nextflow](https://www.nextflow.io/) enables scalable and reproducible scientific workflows using software containers such as Docker and Singularity, and Conda, a package and environment management system. It allows the adaptation of pipelines written in the most common scripting languages suach as such as R and Python. It has a Domain Specific Language (DSL) that simplifies the implementation and the deployment of complex parallel and reactive workflows on clouds and clusters.

## Running Nextflow

Nextflow execution of a workflow must be run on an eddie node which can submit jobs to the scheduler. You can't submit the Nextflow run process as a job with `qsub`, because worker nodes cannot submit new jobs to the scheduler via `qsub`. There are two types of nodes that you can use.

### Login node

You can use a qlogin node to run Nextflow, if you request more than the default 2GB of memory: 8GB is generally enough. It is recommended to do this from a [screen session](https://opensource.com/article/17/3/introduction-gnu-screen) on the head node, so that you can disconnect from eddie while your workflow is running. There is a limit of 48 hours for a qlogin session, which may be a problem for longer running workflows.

```
ssh eddie.ecdf.ed.ac.uk
screen -S nf
qlogin -l h_vmem=8G
nextflow run myworkflow.nf
Ctrl+A+D
```

To resume the screen session and check on your workflow:

```
ssh eddie.ecdf.ed.ac.uk
screen -r nf
```

**Note:** eddie has more than one login (head) node, allocated randomly when you ssh in. Your screen session is specific to the node on which it was started. Make a note of which one you were on when you started your screen session. If you don't see your screen session when you log in again, it's probably because you are on a different login node.

### Wild west node

The MRC Institute of Genetics and Cancer (IGC) owns a set of eddie nodes. The research computing team at the IGC has chosen to designate 3 of these nodes as **wild west nodes**, which have some special properties:

* direct access via SSH
* submission privileges - you can `qsub` jobs from these nodes
* connection to datastore via NFS (same as for staging nodes accessed via `qsub/qlogin -q staging`)
* no memory, CPU, or job run time limitations

These 3 nodes are:

* node2c15
* node2c16
* node3g22

Only members of the IGC AD group on eddie have access to these nodes. If you would like to have something similar for your institute, or university-wide, please bring this up at your next local research computing drop-in session.

To use Nextflow on one of the wild west nodes, the procedure is similar to a qlogin.

```
ssh eddie.ecdf.ed.ac.uk
ssh node3g22
screen -S nf
nextflow run myworkflow.nf
Ctrl+A+D
```

To resume the screen session and check on your workflow (make sure you ssh to the same wild west node!):

```
ssh eddie.ecdf.ed.ac.uk
ssh node3g22
screen -r nf
```

## Configuration

Running Nextflow generally requires a configuration file specific to the high performance computing (HPC) system. Configuration for eddie is based around the Sun Grid Engine (SGE) executor. We have developed a [generic configuration file](https://git.ecdf.ed.ac.uk/igmmbioinformatics/nextflow-eddie/blob/master/config/eddie.config) for use on eddie. We suggest using this configuration file plus one specific to the processes in your own workflow.

Nextflow can use [several different sources of configuration](https://www.nextflow.io/docs/latest/config.html#configuration-file). The priority order is important (1 = highest priority):

1. Parameters specified on the command line (`--something value`)
2. Parameters provided using the `-params-file` option
3. Config file specified using the `-c my_config` option
4. The config file named `nextflow.config` in the current directory
5. The config file named `nextflow.config` in the workflow project directory
6. The config file `$HOME/.nextflow/config`
7. Values defined within the pipeline script itself (e.g. `main.nf`)

**Note:** Parameters set in a pipeline configuration profile specified by `-profile` will take precedence over `nextflow.config` in the current working directory, but not over command line parameters - so between (1) and (4) in the above list, but it's not clear from the documentation if these would have precedence over (2) or (3).

This example command assumes the pipeline-specific configuration is in the `nextflow.config` file in the current directory (which in this case is the same as the workflow project directory). Nexflow will automatically use this configuration file so it does not need to be specified on the command line.

```
nextflow run myworkflow.nf -c eddie.config
```

We base our configuration around using the SGE executor in Nextflow, and have set a default maximum of 100 jobs submitted at any one time, defined in the `executor` block of `eddie.config`.

```
executor {
  name = "sge"
  queueSize = "100"
}
```

### Memory and the shared environment

When you are assigning memory limits to your processes in your `nextflow.config` file, these are by default translated into the `-l h_rss` parameter when using the SGE scheduler. This is **not** how the implementation of SGE on eddie works. Eddie's scheduler uses the `-l h_vmem` parameter in combination with the number of CPU slots specified by `-pe sharedmem` to determine how much memory to allocate. If you have a job that needs a total of 32GB of memory over 4 CPUs, eddie needs to see this specified as:

```
qsub -l h_vmem=8GB -pe sharedmem 4
```

That is, eddie will allocate the amount of memory as `h_vmem * sharedmem`.

If you specified `memory '32GB'` and `cpu 4` in `nextflow.config` for a process, the SGE executor would by default produce:

```
qsub -l h_rss=32GB -pe sharedmem 4
```

Which is completely wrong for eddie. In our generic `eddie.config` file, we solve this with a little mathematics in the `process` block of `eddie.config`.

```
  clusterOptions = { task.memory ? "-l h_vmem=${task.memory.bytes/task.cpus}" : null }
  penv = { task.cpus > 1 ? "sharedmem" : null }
```

The only slight downside to this is that `h_vmem` is specified in bytes rather than in GB, which makes it a little harder to read if you are debugging.

### Memory for processes that run Java

It is highly useful to use the Nextflow variable `task.memory` to give Java commands within processes the maximum heap size via `-Xmx`. In fact, the nf-core pipelines (see below) all do this by default. However, Java processes on eddie require a certain amount of memory overhead on top of this, which means that the SGE scheduler will not allocate enough. To get around this, for processes that use Java, please use add this line to your process specifications in your `nextflow.config` file:

```
clusterOptions = {"-l h_vmem=${(task.memory + 4.GB).bytes/task.cpus}"}
```

For example:
```
process {

withName : "FASTQC" {
    clusterOptions = {"-l h_vmem=${(task.memory + 4.GB).bytes/task.cpus}"}
}

}
```

This will add 4GB to the memory allocation by SGE for the job that runs this process, which is enough to cover the Java overhead.

#### MALLOC\_ARENA\_MAX

This is a technical note explaining why we set the environment variable `$MALLOC_ARENA_MAX` to 1.

```
env {
  MALLOC_ARENA_MAX=1
}
```

Eddie uses `h_vmem` to limit memory usage. That puts a limit on total **virtual memory** allocated to a process. Virtual memory is not the same as actual usage of **physical memory**. Putting an upper limit on virtual memory is at odds with the design assumptions of a lot of programs (including - as it turns out - Java and **glibc**), and arguably of the operating system.
 
Memory virtualization provides a program with the illusion that all the memory it’s working with is one contiguous block of sequential memory addresses. The operating system maintains what’s called a **page table** for each process. That’s a table of in-process memory addresses and where they map to on the physical memory chip. Each entry points to a chunk of memory usually referred to as a memory page. Crucially, entries in this table can be null if the memory has not been written to, or if that block of memory has been moved to the hard drive (this is what swap partitions are for on Unix systems). This is called [demand paging](https://en.wikipedia.org/wiki/Demand_paging).

* Virtual memory usage is the **number of entries in the page table x page size**.
* Actual memory usage (called **rss**) is the **number of pages that are actually in RAM x page size**.
 
From a programmer’s perspective, demand paging provides a bit of a buffer. Your program is free to ask for a large chunk of it and then only fill up the amount it actually needs. This is particularly true of Java which will allocate its entire max heap size at process startup (unless called with the `-Xms` flag), assuming that it won’t actually use all of it.
 
**glibc** is a very low-level utility library which handles, among other things, the process of requesting memory from the operating system via a function called `malloc`. In theory, `malloc` could make a call to the operating system every time you ask for more memory, but frequently programs will ask for many small chunks of memory, e.g. one for each object it’s trying to instantiate. So to be efficient, `malloc` tends to maintain an **arena**, which it grows in slightly larger chunks; an **arena** is just virtual memory until it is actually requested by the operating system.
 
Originally there was one **arena** per program. This caused issues with multi-threaded programs, because if many threads asked for a new block of memory at the same time, they had to wait for `malloc` to deal with their requests one at a time through a locking mechanism. So in order to reduce this contention, **glibc** was modified to give `malloc` one arena per thread, up to a maximum number which apparently by default is the **number of cores on the node x 4**, unless `MALLOC_ARENA_MAX` is set.

If `malloc` allocates an arena for each potential thread, up to a maximum of **number of cores on the node x 4 x 64MB**, where 64MB is the default **arena** size, that works out to a lot of virtual memory (though not physical memory). Java frequently triggers this worst-case behaviour, presumably because it’s designed to be its own virtual machine wrapper around the java process.
 
So for that reason, we set `MALLOC_ARENA_MAX` to a low number. [Some people suggest 4 as a good middle ground for multi-threaded applications](https://thehftguy.com/tag/malloc_arena_max/), but we have chosen 1 as this has worked well in practice.

### Error status and strategy

Nextflow has the ability to re-start processes that fail, based on their exit codes. We allow for some common SGE exit codes to retry process execution in the `process` block of `eddie.config` up to 3 times per instance. You can have [dynamic resource requirements](https://www.nextflow.io/docs/latest/process.html#dynamic-computing-resources) for retried processes. This is best defined in workflow-specific configuration files.

```
  errorStrategy = {task.exitStatus in [143,137,104,134,139,140] ? 'retry' : 'finish'}
  maxRetries = 3
  maxErrors = '-1'
```

### Singularity

To use Singularity containers in your workflow, you will need somewhere to store the downloaded containers. Singularity will attempt to create a directory `.singularity` in your `$HOME` directory on eddie. Space on `$HOME` is very limited, so it is a good idea to create a directory somewhere else with more room and link the locations. Don't put it in `/exports/eddie/scratch/UUN` because space there is also limited.

```
cd $HOME
mkdir /exports/inst/eddie/fileset/path/to/my/area/.singularity
ln -s /exports/inst/eddie/fileset/to/my/area/.singularity .singularity
```

To use Singularity in your workflow, begin by loading it from the module system. You can add these lines to the file `$HOME/.bashrc` on eddie, or you can run the commands before you run your workflow.

```
module load singularity
```

Singularity is enabled by default in our generic `eddie.config` file with these lines in the `process` block.

```
beforeScript =
  """
  . /etc/profile.d/modules.sh
  module load 'singularity'
  export SINGULARITY_TMPDIR="\$TMPDIR"
  """
```

And these lines in the `singularity` block.

```
singularity {
  envWhitelist = "SINGULARITY_TMPDIR,TMPDIR"
  runOptions = '-p -B "$TMPDIR"'
  enabled = true
  autoMounts = true
}
```

### TMPDIR

Note that there are a number of lines referring to `$TMPDIR` in the Singularity section. On eddie, `$TMPDIR` points to a dynamically allocated local folder on the node, which SGE deletes after job completion. It should have plenty of space available. Eddie nodes have a small `$TMP` which is not intended for users. `$TMPDIR` is set per-job on worker nodes, and programs are supposed to respect that and use it.

Nextflow will use `$TMPDIR` for local execution if the process directive `scratch` is set to `true`, as it is in `eddie.config`. See [the Nextflow documentation](https://www.nextflow.io/docs/latest/process.html#scratch) for details.

Nextflow runs Singularity with all environment variables wiped. If you look at the `command.run` script in any of the working directories of a Nextflow run, you’ll see the call to Singularity wrapped in an `env –`. Singularity unpacks containers to `/tmp` unless `$TMPDIR` is set.

The Singularity modules on eddie have been installed with default configuration options, which include the option to mount `/tmp` inside the container. Because Nextflow wipes these environment variables, processes inside the container write to `/tmp` because `/tmp` is mounted by the system and `$TMPDIR` is not set.

* `export SINGULARITY_TMPDIR="\$TMPDIR"` in the `beforeScript` block in `process` ensures Singularity unpacks containers into `$TMPDIR` rather than `/tmp` before running the process
* `envWhitelist` makes sure `$TMPDIR` is passed to singularity, but also sets `$TMPDIR` inside the container
* `runOptions '-B "$TMPDIR"'` sees to it that `$TMPDIR` is mounted inside the container

Finally, the `-p` option in `runOptions` enables PID isolation. This is needed because container engines are seen by child processes as the OS, meaning that processes within containers have their parent pid set to 0. ECDF runs a script on eddie that will kill any orphaned processes which runs every two hours, and unfortunately it tends to think some processes running within containers are orphaned.

### Conda

Nextflow supports the use of Conda packages, but we recommend using Singularity where possible. If you must use it, you will need to load it via the module system either in your `$HOME/.bashrc` or on the command line prior to running your workflow.

```
module load anaconda/5.3.1
```

Use the generic configuration file [`eddie_conda.config`](https://git.ecdf.ed.ac.uk/igmmbioinformatics/nextflow-eddie/blob/master/config/eddie_conda.config) rather than `eddie.config`. Note it lacks the `singularity` block, and that the `beforeScript` block is changed to load Conda instead of Singularity for processes.

```
  beforeScript =
  """
  . /etc/profile.d/modules.sh
  module load anaconda/5.3.1
  """
```

### nf-core pipelines

[nf-core](https://nf-co.re/) is a community effort to build and collect a curated set of Nextflow analysis pipelines for bioinformatics. Popular pipelines include [sarek](https://nf-co.re/sarek) for alignment and variant calling (germline and somatic), [rnaseq](https://nf-co.re/rnaseq), [chipseq](https://nf-co.re/chipseq), and [fetchngs](https://nf-co.re/fetchngs). One of the handy things about nf-core pipelines is the use of [institutional configurations](https://github.com/nf-core/configs). We have written one of these for eddie, and there is [detailed documentation on how to use it](https://github.com/nf-core/configs/blob/master/docs/eddie.md). A number of the pipelines (but not all!) support pipeline-specific institutional configuration files, and we have contributed eddie configurations for some of these, primarily to handle the Java memory overhead issue for processes that require it.